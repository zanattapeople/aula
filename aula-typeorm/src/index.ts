import "reflect-metadata";
import { createConnection } from "typeorm";
import { Fabricante } from "./entity/Fabricante";
import { Produto } from "./entity/Produto";

createConnection().then( async connection => {

  console.log( "Conexão OK!" )

  connection.getRepository( Fabricante ).findOne( {
    where: {
      idFabricante: 2
    }
  } ).then( rsFabricante => {

    if ( !rsFabricante ) {

      console.log( "Fabricante Não Encontrado...." )

    } else {

      console.log( 'Fabricante Encontrado: ', rsFabricante )

      // connection.close()

      connection.getRepository( Produto ).find( {
        where: {
          idFabricante: 2
        }
      } ).then( rsProdutos => {

        console.log( 'Produto Encontrado: ', rsProdutos )

      } ).catch( err => {
        console.log( "Erro do Find do Produto...." )
      } )

    }

  } ).catch( err => {
    console.log( "Erro do FindOne do Fabricante...." )
  } )

  /*
  connection.getRepository( Fabricante ).findOne( {
    relations: ['produtos'],
    where: {
      idFabricante: 2
    }    
  } ).then( rsFabricante => {
    console.log( "\n\n\n\n\n" )
    console.log( "Achei um Fabricante...." )
    console.log( "=======================" )
    console.log( rsFabricante.nome )
    console.log( "======================= RS FABRICANTE...." )
    console.log(rsFabricante)
  } )
*/

  /*
  connection.getRepository(Produto).save({
    descricao: "Cpu",
    partnumber: 'partnumbercpu',
    sku: 'skucpu',
    idFabricante: 2
  })

  connection.getRepository(Produto).save({
    descricao: "Monitor 19.5",
    partnumber: 'blablabla',
    sku: 'blablabla',
    idFabricante: 2
  })
*/
  /*
  connection.getRepository( Fabricante ).save( {
    nome: "Philips"
  } )

  connection.getRepository( Fabricante ).save( {
    nome: "Lenovo"
  } )

  connection.getRepository( Fabricante ).save( {
    nome: "Asus"
  } )

  /*

  connection.getRepository(Fabricante).save({
    nome: 'AOC'
  })


    console.log("Inserting a new user into the database...");
    const user = new User();
    user.firstName = "Augustus";
    user.lastName = "Saw";
    user.age = 25;
    await connection.manager.save(user);
    console.log("Saved a new user with id: " + user.id);

    console.log("Loading users from the database...");
    const users = await connection.manager.find(User);
    console.log("Loaded users: ", users);

    console.log("Here you can setup and run express/koa/any other framework.");

    */

} ).catch( error => console.log( error ) );

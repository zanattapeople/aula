import { Column, CreateDateColumn, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Produto } from "./Produto";

@Entity( { name: 'fabricantes' } )
export class Fabricante {

  @PrimaryGeneratedColumn()
  idFabricante: number;

  @Column( { length: 35 } )
  nome: string;

  /*
  @OneToMany( () => Produto, produto => produto.fabricante )
  produtos: Produto[]
  */

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

}

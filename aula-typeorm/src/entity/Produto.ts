import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { Fabricante } from "./Fabricante";

@Entity( { name: 'produtos' } )
export class Produto {

  @PrimaryGeneratedColumn()
  idProduto: number;

  @Column( { length: 100 } )
  descricao: string;

  @Column( { default: true } )
  ativo: boolean;

  @Column( { length: 100, nullable: true } )
  partnumber: string;

  @Column( { length: 100, nullable: true } )
  sku: string;

  /*
  @ManyToOne( () => Fabricante, x => x.produtos )
  @JoinColumn( { name: 'idFabricante' } )
  fabricante: Fabricante
  */

  @Column()
  @JoinColumn( { name: 'idFabricante', referencedColumnName: 'idFabricante' } )
  @ManyToOne( () => Fabricante ) // Retirado caso venhamos a utilizar o ManyToOne com cadastro de fabricante
  idFabricante: number

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

}

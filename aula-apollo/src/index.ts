import "reflect-metadata"
import express from "express"
import { ApolloServer } from "apollo-server-express"
import { buildSchema } from "type-graphql"
import { ExemploResolver } from "./resolvers/exemploResolver"
import http from 'http'

const PORT_ENDPOINT: number = 4000;

( async () => {

  const app = express()

  const apolloServer = new ApolloServer( {
    playground: true,
    schema: await buildSchema( {
      resolvers: [ExemploResolver],
      validate: false,
    } )
  } )

  apolloServer.applyMiddleware( { app } )

  //TODO - Colocar em HTTPs

  let server: http.Server = http.createServer(
    app
  )

  server.listen( { port: PORT_ENDPOINT, }, () => {

    console.log( '🚀 Server ready Port: '.concat( PORT_ENDPOINT.toString() ) )

    // server.close()

  } )

} )()
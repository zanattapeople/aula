const express = require( 'express' );
const { ApolloServer, gql } = require( 'apollo-server-express' );

function retornarUmaString () {
  return { nome: "Nome do Cliente", endereco: "Endereço" }
}

// Construct a schema, using GraphQL schema language
const typeDefs = gql`
  type Query {
    AnaBeatriz: String
    hello: typeHello
    Augusto: String
  }

  type typeHello {
      nome: String
      endereco: String
  }
`;

// Provide resolver functions for your schema fields
const resolvers = {
  Query: {
    AnaBeatriz: () => 'You Are Welcome! - Dentro do Modelo Puro',
    hello: () => retornarUmaString(),
    Augusto: () => 'Resultado do Augusto dentro do Modelo Puro'
  }
};

const server = new ApolloServer( { typeDefs, resolvers } );

const app = express();
server.applyMiddleware( { app } );

app.listen( { port: 5000 }, () =>
  console.log( `🚀 Server ready at http://localhost:5000${server.graphqlPath}` )
);
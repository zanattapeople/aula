
( async () => {

  console.log( "01 - Entrei dentro do Async...." )

  let x = await imprimirNome( 'DENTRO DO ASYNC' )

  console.log( "02 - Resultado de x: ", x )

} )()


async function imprimirNome ( qualNome: string ) {
  console.log( "03 - Dentro de Imprimir Nome: ", qualNome )
  return "04 - Retorno de Imprimir Nome!!!!"
}

console.log( "05 - Vou Executar Imprimir Nome.... na Raiz" )

imprimirNome( "CHAMANDO PELA RAIZ" ).then( rs => {
  console.log( '06 - Resultado RS dentro do THEN: ', rs )

} ).catch( err => {
  console.log( 'Recebi erro Resulgado: ', err )
} )

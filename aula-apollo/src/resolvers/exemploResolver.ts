import { Resolver, Mutation, Query } from "type-graphql";

@Resolver()
export class ExemploResolver {

  @Mutation( () => String )
  async hello (): Promise<String> {
    return "Oi..."
  }

  @Query( () => String )
  async AnaBeatriz (): Promise<String> {
    return "You Are Welcome"
  }

  @Query( () => String )
  async Augusto (): Promise<String> {
    return "Tchau..."
  }

}

/*

  @Authorized()
  @Mutation( () => RespostaPadraoType )
  async logout ( @Ctx() context: ContextoInterface ): Promise<RespostaPadraoType> {
    return new ClsLogin().logout( context.acesso.idUsuario )
  }

  @Authorized()
  @Query( () => RespostaPadraoType )
  async veriticaPermissao ( @Arg( "permissao" ) permissao: VerificaPermissaoInput ): Promise<RespostaPadraoType> {
    return new ClsAcesso().verificaPermissao( permissao )
  }


  */

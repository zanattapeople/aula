import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql"
import https from 'https'
import express from "express"
import "reflect-metadata"
import { ExemploResolver } from "./resolvers/exemploResolver";

const PORT_ENDPOINT: number = 4000;

( async () => {

  const app = express();

  const apolloServer = new ApolloServer( {
    schema: await buildSchema( {
      resolvers: [ExemploResolver],
      validate: false,
    } ),
    /*
    context: ( { req, res } ): Promise<ContextoInterface> => {

      let usuarioLogado: AcessoType = {
        idUsuario: 0,
        ok: false,
        mensagem: 'Login Inválido!',
        tipo: TipoUsuarioRole.VENDEDOR
      }

      // let token: string = <string>req.headers.token || ''

      let token = req.headers.authorization || '';

      if ( token.includes( 'Bearer' ) ) {
        token = token.replace( 'Bearer ', "" ).trim()
      }

      // console.log( '\n\n\ntoken:"', token, '"\n\n\n' )
      // console.log(token.length)

      if ( token ) {
        return new ClsAcesso().ChecarTokenValido( token ).then( rs => {
          if ( rs.ok ) {
            return { req, res, acesso: rs, clsWhatsApp: clsWhatsApp }
          } else {
            return Promise.resolve( { req, res, acesso: usuarioLogado, clsWhatsApp: clsWhatsApp } )
          }
        } )
      } else {
        return Promise.resolve( { req, res, acesso: usuarioLogado, clsWhatsApp: clsWhatsApp } )
      }
    }
    */


  } );

  // apolloServer.applyMiddleware( { app, cors: setupCors } );
  apolloServer.applyMiddleware( { app } );

  // Roda Em Desenvolvimento Sem SSL
  /*
  if ( EMDESENVOLVIMENTO ) {

    app.listen( PORT_ENDPOINT, () => {
      console.log( `server started at http://localhost:${PORT_ENDPOINT}/graphql` );
    } );

  } else {
*/
  // Create the HTTPS or HTTP server, per configuration
  var server
  // Assumes certificates are in a .ssl folder off of the package root. Make sure 
  // these files are secured.
  server = https.createServer(
    /*
    {
      // key: fs.readFileSync( `./ssl/${environment}/server.key` ),
      key: fs.readFileSync( `/etc/letsencrypt/live/peopledivinopolis.com.br/privkey.pem` ),
      //cert: fs.readFileSync( `./ssl/${environment}/server.crt` )
      cert: fs.readFileSync( `/etc/letsencrypt/live/peopledivinopolis.com.br/cert.pem` )
    },
    */
    app
  )

  server.listen( { port: PORT_ENDPOINT }, () =>
    console.log(
      '🚀 Server ready Port: '.concat( PORT_ENDPOINT.toString() ),
    )
  )
} )();

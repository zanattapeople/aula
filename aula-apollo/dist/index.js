"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const type_graphql_1 = require("type-graphql");
const https_1 = __importDefault(require("https"));
const express_1 = __importDefault(require("express"));
require("reflect-metadata");
const PORT_ENDPOINT = 4000;
(async () => {
    const app = express_1.default();
    const apolloServer = new apollo_server_express_1.ApolloServer({
        schema: await type_graphql_1.buildSchema({
            resolvers: [],
            validate: false,
        }),
    });
    apolloServer.applyMiddleware({ app });
    var server;
    server = https_1.default.createServer(app);
    server.listen({ port: PORT_ENDPOINT }, () => console.log('🚀 Server ready Port: '.concat(PORT_ENDPOINT.toString())));
})();

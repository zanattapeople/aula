"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const express_1 = __importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const type_graphql_1 = require("type-graphql");
const GrupoResolver_1 = require("./resolvers/GrupoResolver");
const UsuarioResolver_1 = require("./resolvers/UsuarioResolver");
const ClienteResolver_1 = require("./resolvers/ClienteResolver");
const LoginResolver_1 = require("./resolvers/LoginResolver");
const ClsAutenticacao_1 = require("./auth/ClsAutenticacao");
const ClsAcesso_1 = __importDefault(require("./resolvers/ClsAcesso"));
const https_1 = __importDefault(require("https"));
const fs_1 = __importDefault(require("fs"));
const emDesenvolvimento_1 = require("./config/emDesenvolvimento");
const RolesInterfaces_1 = require("./interfaces/RolesInterfaces");
const OrigemResolver_1 = require("./resolvers/OrigemResolver");
const LeadResolver_1 = require("./resolvers/LeadResolver");
const ProdutoResolver_1 = require("./resolvers/ProdutoResolver");
const NotificacaoResolver_1 = require("./resolvers/NotificacaoResolver");
const RelatoriosResolver_1 = require("./resolvers/RelatoriosResolver");
const AgendaResolver_1 = require("./resolvers/AgendaResolver");
const zlib_whats_1 = require("zlib-whats");
const WhatsAppResolver_1 = require("./resolvers/WhatsAppResolver");
(async () => {
    const setupCors = {
        credentials: true,
        origin: [
            "http://crmvendas.fleekcursos.com.br",
            "https://crmvendas.fleekcursos.com.br",
            "http://fleekcursos.com.br",
            "https://fleekcursos.com.br",
            "http://localhost",
            "http://localhost:8080"
        ],
        maxAge: 86400,
        headers: [
            'Origin',
            'X-Requested-With',
            'Content-Type',
            'Accept',
            'Authorization'
        ]
    };
    const app = express_1.default();
    const options = await typeorm_1.getConnectionOptions(process.env.NODE_ENV || "development");
    await typeorm_1.createConnection({ ...options, name: "default" });
    const clsWhatsApp = new zlib_whats_1.ClsWhatsApp();
    const apolloServer = new apollo_server_express_1.ApolloServer({
        schema: await type_graphql_1.buildSchema({
            resolvers: [
                AgendaResolver_1.AgendaResolver,
                ClienteResolver_1.ClienteResolver,
                GrupoResolver_1.GrupoResolver,
                LeadResolver_1.LeadResolver,
                LoginResolver_1.LoginResolver,
                NotificacaoResolver_1.NotificacaoResolver,
                OrigemResolver_1.OrigemResolver,
                ProdutoResolver_1.ProdutoResolver,
                RelatoriosResolver_1.RelatorioResolver,
                UsuarioResolver_1.UsuarioResolver,
                WhatsAppResolver_1.WhatsAppResolver
            ],
            validate: false,
            authChecker: ClsAutenticacao_1.customAuthChecker
        }),
        context: ({ req, res }) => {
            let usuarioLogado = {
                idUsuario: 0,
                ok: false,
                mensagem: 'Login Inválido!',
                tipo: RolesInterfaces_1.TipoUsuarioRole.VENDEDOR
            };
            let token = req.headers.authorization || '';
            if (token.includes('Bearer')) {
                token = token.replace('Bearer ', "").trim();
            }
            if (token) {
                return new ClsAcesso_1.default().ChecarTokenValido(token).then(rs => {
                    if (rs.ok) {
                        return { req, res, acesso: rs, clsWhatsApp: clsWhatsApp };
                    }
                    else {
                        return Promise.resolve({ req, res, acesso: usuarioLogado, clsWhatsApp: clsWhatsApp });
                    }
                });
            }
            else {
                return Promise.resolve({ req, res, acesso: usuarioLogado, clsWhatsApp: clsWhatsApp });
            }
        }
    });
    apolloServer.applyMiddleware({ app, cors: setupCors });
    if (emDesenvolvimento_1.EMDESENVOLVIMENTO) {
        app.listen(emDesenvolvimento_1.PORT_ENDPOINT, () => {
            console.log(`server started at http://localhost:${emDesenvolvimento_1.PORT_ENDPOINT}/graphql`);
        });
    }
    else {
        var server;
        server = https_1.default.createServer({
            key: fs_1.default.readFileSync(`/etc/letsencrypt/live/peopledivinopolis.com.br/privkey.pem`),
            cert: fs_1.default.readFileSync(`/etc/letsencrypt/live/peopledivinopolis.com.br/cert.pem`)
        }, app);
        server.listen({ port: emDesenvolvimento_1.PORT_ENDPOINT }, () => console.log('🚀 Server ready Port: '.concat(emDesenvolvimento_1.PORT_ENDPOINT.toString())));
    }
})();

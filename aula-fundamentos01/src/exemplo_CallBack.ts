let alunos: Array<string> = ['Ana', 'Augustus', 'Webert']

let quantidade: number = 10

const fnParaMaiusculo = function ( oque: string ): string {
  return oque.toUpperCase()
}

const minhaFuncaoCallBack = function exibirNome ( valor: string, indice: number, arrayOriginal: Array<string> ) {
  console.log( indice, fnParaMaiusculo( valor ) )
}

function imprimirMeuNome ( nome: string, fn: any ) {

  console.log( '\n\n\n====================' )
  console.log( "O Nome Original é: ".concat( nome ) )
  console.log( fn( nome ) )
  console.log( '\n\n\n\n\n' )
}

alunos.forEach( minhaFuncaoCallBack )

imprimirMeuNome( 'Zanatta', fnParaMaiusculo )

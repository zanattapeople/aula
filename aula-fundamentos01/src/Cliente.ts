export default class Cliente {

  private nome: string
  private tmpEndereco: string

  constructor( qualNome: string, qualEndereco: string = 'Valor Padrao do Endereco' ) {
    this.nome = qualNome
    this.tmpEndereco = qualEndereco
  }

  // Declaração de Método
  public setNome ( qualNome: string ): void {
    this.nome = qualNome
  }

  public getNome (): string {
    console.log( 'Dentro de getNome....' )
    return this.nome
  }

  // Declaração de Propriedade
  public set endereco ( qualEndereco: string ) {
    this.tmpEndereco = qualEndereco
  }

  public get endereco (): string {
    return this.tmpEndereco
  }

  public gravarClienteComCallBack ( idade: number, fnCallBack: ( rs: boolean, mensagem?: string ) => void ): void {

    if ( idade >= 18 ) {

      console.log( "CLASSE CLIENTE - Gravando - Cliente Maior que 18..." )
      fnCallBack( true )

    } else {

      console.log( "CLASSE CLIENTE - Erro - Cliente Menor que 18..." )
      fnCallBack( false, 'CLASSE CLIENTE - Mensagem de Erro Retornada - Permitido somente para Maiores....' )

    }

  }

  public gravarCliente ( idade: number ): Promise<boolean> {

    /*
    return new Promise( function ( resolve, reject ) {
    })
    */

    return new Promise( ( resolve, reject ) => {

      if ( idade >= 18 ) {
        console.log( "CLASSE CLIENTE - Gravando - Cliente Maior que 18..." )
        resolve( true )
      } else {
        console.log( "CLASSE CLIENTE - Erro - Cliente Menor que 18..." )
        reject( new Error( 'CLASSE CLIENTE - Mensagem de Erro Retornada - Permitido somente para Maiores....' ) )
      }

    } )

  }

}
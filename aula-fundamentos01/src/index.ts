import Cliente from "./Cliente";

let clsCliente: Cliente = new Cliente( 'Webert', 'Rio Grande do Sul, 1350' )

let novoCliente: Cliente = new Cliente( 'Zanata' )

let clienteGravadoComSucesso: boolean = false

/*
console.log( clsCliente.getNome() )
console.log( clsCliente.endereco )

clsCliente.setNome( 'Ana Beatriz' )
clsCliente.endereco = 'Rio de Janeiro 923'

console.log( clsCliente.getNome() )
console.log( clsCliente.endereco )

console.log( "\n\n\n\n\n==================================================\n\n\n\n\n\n" )

clsCliente.gravarCliente( 25 ).then( result => {
  if ( result ) {
    console.log( "\n\nGRAVAR CLIENTE - RESULT OK - Deu Certo - Dentro da Chamada" )
    clienteGravadoComSucesso = true
  }
} ).catch( err => {
  clienteGravadoComSucesso = false
  console.log( "\n\nGRAVAR CLIENTE - RESULT FALSE - Deu Erro Dentro do", err.message )
} )

console.log( "Consigo saber se gravou ou não????? Resultado: ", clienteGravadoComSucesso )

console.log( "\n\n\n\n\n==================================================\n\n\n\n\n\n" )



novoCliente.gravarClienteComCallBack( 25, ( rs, mensagem ) => {
  if ( rs ) {
    console.log( "Deu Certo Fazendo com CallBack...." )
  } else {
    console.log( "Deu Errado com Call Back - Mensagem de Retorno: ".concat( mensagem ? mensagem : '' ) )
  }
} )


console.log( "\n\n\n\n===================\nTerminei o Programa" )

*/
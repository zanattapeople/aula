import "reflect-metadata"
import { createConnection, getConnectionOptions } from "typeorm"
import express from "express"
import { ApolloServer } from "apollo-server-express"
import { buildSchema } from "type-graphql"
// import { BookResolver } from "./resolvers/BookResolver.ts" // add this

const PORT_ENDPOINT: number = 4000;

( async () => {

  const app = express();


  const options = await getConnectionOptions(
    process.env.NODE_ENV || "development"
  );

  await createConnection( { ...options, name: "default" } );

  const apolloServer = new ApolloServer( {
    schema: await buildSchema( {
      // resolvers: [GrupoResolver, LoginResolver, UsuarioResolver, ClienteResolver, OrigemResolver],
      resolvers: [],
      validate: false,
      // authChecker: customAuthChecker
    } ),
  } )

  apolloServer.applyMiddleware( { app } );


  app.listen( PORT_ENDPOINT, () => {
    console.log( `server started at http://localhost:${PORT_ENDPOINT}/graphql` );
  } );

} )()
import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from "typeorm";
import { Field, ObjectType } from 'type-graphql';
import { OrigemInterface } from "../interfaces/OrigemInterface";

@ObjectType()
@Entity( "cliente" )
export class Origem implements OrigemInterface {
  @Field( { nullable: false } )
  @PrimaryGeneratedColumn( { name: "idOrigem", type: "int" } )
  idOrigem: number

  @Field( { nullable: true } )
  @Column( "varchar", { length: 40, nullable: true } )
  descricao: string

  @Field( { nullable: true } )
  @Column( "decimal", { default: 0 } )
  custo: number

  @Field( { nullable: true } )
  @Column( "boolean", { default: true } )
  ativa: boolean

  @CreateDateColumn( { name: "createdAt", nullable: false } )
  createdAt: Date;

  @UpdateDateColumn( { name: "updatedAt", nullable: false } )
  updatedAt: Date;

}

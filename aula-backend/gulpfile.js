const { src, dest, parallel, series } = require( 'gulp' );
// const minifyCSS = require( 'gulp-csso' );
// const concat = require( 'gulp-concat' );
const gpTs = require( 'gulp-typescript' )
const gpSourcemaps = require( 'gulp-sourcemaps' );
const gpTsProject = gpTs.createProject( 'tsconfig.json' )
const del = require( 'del' );
// const gulp = require( 'gulp' )

const sourceMap = true
const DESTINO = 'dist'
const DESTINO_CONFIG = 'dist'
const ORIGEM = 'src/'

function typescript () {

  return src( ORIGEM.concat( '**/*.ts' ) )
    // TODO - ALTERAR EM PRODUCAO - Retirar em Producao
    // .pipe( gpSourcemaps.init() )
    .pipe( gpTsProject() )
    .on( 'error', function ( err ) {
      console.log( "ERRO DENTRO DO GULP....." )
      console.log( err.toString() );

      this.emit( 'end' );
    } )
    .pipe( gpSourcemaps.write() )
    .pipe( dest( DESTINO, { sourcemaps: sourceMap } ) )
}

function javascript () {
  return src( ORIGEM.concat( '**/*.js' ), { sourcemaps: sourceMap } )
    // .pipe( concat( 'app.min.js' ) )
    .pipe( dest( DESTINO, { sourcemaps: sourceMap } ) )
}

function json () {
  return src( ORIGEM.concat( '**/*.json' ) )
    // .pipe( concat( 'app.min.js' ) )
    .pipe( dest( DESTINO ) )
}

function imagens () {
  return src( ORIGEM.concat( '**/*.jpg' ) )
    // .pipe( concat( 'app.min.js' ) )
    .pipe( dest( DESTINO ) )
}

function html () {
  return src( ORIGEM.concat( '**/*.html' ) )
    // .pipe( concat( 'app.min.js' ) )
    .pipe( dest( DESTINO ) )
}

function clear () {
  return del( [ DESTINO ] )
}

function orm () {
  return src( "./ormconfig.js" )
    // .pipe( rename( "ormconfig.json" ) )
    .pipe( dest( DESTINO_CONFIG ) );
}

function config () {
  return src( "./package.json" )
    // .pipe( rename( "ormconfig.json" ) )
    .pipe( dest( DESTINO_CONFIG ) );
}

// 

// exports.js = js;
// exports.ts = ts;
exports.limpar = clear
exports.default = series( clear, parallel( typescript, javascript, json, imagens, html, orm, config ) );

module.exports = [
  {
    name: "development",
    type: "mysql",
    username: "usuarioaula",
    password: "senhaaula",
    database: "bancoaula",
    host: "191.252.221.55",
    port: 3306,
    synchronize: true,
    logging: false,
    entities: [ "src/entity/*.ts" ],
    migrations: [ "src/migration/**/*.ts" ],
    subscribers: [ "src/subscriber/**/*.ts" ],
    cli: {
      entitiesDir: "src/entity",
      migrationsDir: "src/migration",
      subscribersDir: "src/subscriber"
    }
  },
  {
    name: "production",
    type: "mysql",
    username: "usuarioaula",
    password: "senhaaula",
    database: "bancoaula",
    host: "localhost",
    port: 3306,
    synchronize: true, // switch this to false once you have the initial tables created and use migrations instead
    logging: false,
    entities: [ "dist/entity/**/*.js", "entity/**/*.js" ],
    migrations: [ "dist/migration/**/*.js", "migration/**/*.js" ],
    subscribers: [ "dist/subscriber/**/*.js", "subscriber/**/*.js" ],
    cli: {
      entitiesDir: "dist/entity",
      migrationsDir: "dist/migration",
      subscribersDir: "dist/subscriber"
    }
  }
];

import Vue from 'vue'
import Component from 'vue-class-component'

import ClsDados from './ClsDados'

@Component
export default class App extends Vue {

  private nome: string = ''

  private clsDados: ClsDados = new ClsDados()

  private nomeMaiusculo (): string {
    return this.nome.toUpperCase()
  }

  private salvarDados (): void {

    this.clsDados.setNome( this.nome )

    console.log( " ESTOU DENTRO DO SALVAR DADOS....Salvar Dados!!!!", this.clsDados.getNome() )
  }

}
export default class ClsAugusto {

  private tmpNome: string = ''

  public setNome ( qualNome: string ) {
    this.tmpNome = qualNome
  }

  public getNome (): string {
    return this.tmpNome.toLowerCase()
  }

  public salvar (): void {
    console.log( "Salvando os dados.... etc, etc...." )
  }
}